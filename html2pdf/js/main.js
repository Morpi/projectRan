var arrayOfSections = [];
for(var ids=0; ids<$('.page').length; ids++) {
	arrayOfSections.push('#'+$('.page')[ids].id);
}

for(var s=0; s<arrayOfSections.length; s++) {
	var containerHeight;
	var tableRowHeaderHeight;
	var tableContentHeight;
	var arrayOfElementsHeight = [];
	var elementHeight = 0;
	var nextPage = 1;
	var pageClone;
	var currentContainerIdName = arrayOfSections[s].slice(0, -1);	
	
	var y = $(arrayOfSections[s]+' .table__container').find(".table__content .table__row");
	for (var i=0; i < y.length; i++) {
		var isFlagsRow = $('#table-container-1 .flags .table__row');

		if($(currentContainerIdName+nextPage+' .table__container').height() < $(isFlagsRow[i]).outerHeight()) {
			var flagsId = $(isFlagsRow[i]).attr("id", ('domainFlags-1'));
			var flagsElement;
			var flagsHeight = 0;
			var flagsStep = 1;
			var flagsClone;

			var rowFlagsArray = [];
			var f = $(flagsId).find("span");
			for (var fi=0; fi < f.length; fi++) {
				rowFlagsArray.push(f[fi]);
				$(f[fi]).remove();
			}

			for (var z=0; z < rowFlagsArray.length; z++) {
				flagsElement = $('#domainFlags-'+flagsStep);

				if(flagsHeight<152) {					
					flagsHeight = flagsElement.outerHeight();
					flagsElement.find('.table__flags').append(rowFlagsArray[z]);
				} else {
					flagsClone = flagsElement.clone().attr("id", ('domainFlags-'+(flagsStep+1)));
					$(flagsClone).find("span").remove();
					$(flagsElement).after(flagsClone);
					flagsStep += 1;
					z -= 1;
					flagsHeight = 0;
				}
			}		
		}
	}

	var rowsArray = [];
	var ny = $(arrayOfSections[s]+' .table__container').find(".table__content .table__row");
	for (var i=0; i < ny.length; i++) {
		arrayOfElementsHeight.push($(ny[i]).outerHeight());
		rowsArray.push(ny[i]);
		$(ny[i]).remove();
	}

	for(var itr=0; itr<rowsArray.length; itr++) {
		containerHeight = $(currentContainerIdName+nextPage+' .table__container').height();
		tableRowHeader = $(arrayOfSections[s]+' .table__row-header');
		tableRowHeaderHeight = tableRowHeader.outerHeight();
		tableContentHeight = containerHeight - tableRowHeaderHeight;
		cloneOfTableRowHeader = tableRowHeader.clone();

		if (tableContentHeight>elementHeight) {
			if(tableContentHeight>elementHeight+arrayOfElementsHeight[itr]) {
				$(currentContainerIdName+nextPage+" #table-container-"+nextPage+' .table__content').append(rowsArray[itr]);
				elementHeight += arrayOfElementsHeight[itr];            
			} else {
				elementHeight += arrayOfElementsHeight[itr];
				itr -= 1;
			}
		} else {
			pageClone = $(currentContainerIdName+nextPage).clone().attr("id", arrayOfSections[s].slice(1, -1)+(nextPage+1));

			$(pageClone).find("#table-container-"+nextPage).attr("id", "table-container-"+(nextPage+1));
			$(pageClone).find(".table__row").remove();
			$(pageClone).find(".details").remove();

			$(currentContainerIdName+nextPage).after(pageClone).after("<div class='next-page'></div>");

			nextPage += 1;
			elementHeight = arrayOfElementsHeight[itr];

			$(currentContainerIdName+nextPage+' .table').addClass('table-full');
			$(currentContainerIdName+nextPage+" #table-container-"+nextPage).prepend(cloneOfTableRowHeader[0]);
			$(currentContainerIdName+nextPage+" #table-container-"+nextPage+' .table__content').append(rowsArray[itr]);
		}

	}
}

// var arrayOfSections = []
    // for(var ids=0; ids<$('.page').length; ids++) {
    //     arrayOfSections.push('#'+$('.page')[ids].id)
    // }

    // for(var s=0; s<arrayOfSections.length; s++) {
    //     var containerHeight = 0;
    //     var tableRowHeaderHeight = 0;
    //     var tableContentHeight = 0;

    //     var arrayOfElementsHeight = [];
    //     $(arrayOfSections[s]+' .table__content .table__row').each(function() {
    //         arrayOfElementsHeight.push($(this).outerHeight())
    //     })

    //     var elementHeight = 0;
    //     var nextPage = 1;
    //     var currentPage = 0;

    //     var rowsArray = [];
    //     var y = $(arrayOfSections[s]+' .table__container').find(".table__content .table__row");
    //     for (var i=0; i < y.length; i++) {
    //         rowsArray.push(y[i]);
    //         $(y[i]).remove();
    //     }

    //     var pageClone;
    //     var currentContainerIdName = arrayOfSections[s].slice(0, -1)

    //     for(var itr=0; itr<y.length; itr++) {
    //         containerHeight = $(currentContainerIdName+nextPage+' .table__container').height();
    //         tableRowHeader = $(arrayOfSections[s]+' .table__row-header')
    //         tableRowHeaderHeight = tableRowHeader.outerHeight()
    //         tableContentHeight = containerHeight - tableRowHeaderHeight
    //         cloneOfTableRowHeader = tableRowHeader.clone()

    //         if (tableContentHeight>elementHeight) {
    //             if(tableContentHeight>elementHeight+arrayOfElementsHeight[itr]) {
    //                 $(currentContainerIdName+nextPage+" #table-container-"+nextPage+' .table__content').append(rowsArray[itr]);
    //                 elementHeight += arrayOfElementsHeight[itr];            
    //             } else {            
    //                 elementHeight += arrayOfElementsHeight[itr]
    //                 itr -= 1
    //             }
    //         } else {
    //             pageClone = $(currentContainerIdName+nextPage).clone().attr("id", arrayOfSections[s].slice(1, -1)+(nextPage+1));

    //             $(pageClone).find("#table-container-"+nextPage).attr("id", "table-container-"+(nextPage+1))
    //             $(pageClone).find(".table__row").remove();
    //             $(pageClone).find(".details").remove();

    //             $(currentContainerIdName+nextPage).after(pageClone).after("<div class='next-page'></div>");

    //             nextPage += 1;
    //             elementHeight = arrayOfElementsHeight[itr];

    //             $(currentContainerIdName+nextPage+' .table').addClass('table-full');
    //             $(currentContainerIdName+nextPage+" #table-container-"+nextPage).prepend(cloneOfTableRowHeader[0]);
    //             $(currentContainerIdName+nextPage+" #table-container-"+nextPage+' .table__content').append(rowsArray[itr]);
    //         }
    //     }
    // }