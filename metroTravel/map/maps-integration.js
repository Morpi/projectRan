function initMap() {
    try {
      var locations_list = [];

      const retrievedObject = localStorage.getItem('metroweb_response');
      const metroweb_response = JSON.parse(retrievedObject).pois;

      for (const location in metroweb_response) {
        const location_latitude = metroweb_response[location]["latitude"];
        const location_longitude = metroweb_response[location]["longitude"];
        const location_icon = metroweb_response[location]["icon"];
  
        let location_marker = {
          position: new google.maps.LatLng(`${location_latitude}`, `${location_longitude}`),
          type: location_icon,
        }
        
        locations_list.push(location_marker);

      }

    } catch (error) {
      console.log(error);
    }

    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: 31.4104627, lng: 34.2486091 },
      zoom: 10,
    });

    const iconBase = "http://metrotravel01.zero21dev.com/map_icons/"

    const icons = {
        attraction: {
          icon: iconBase + "attractions.png",
        },
        culture: {
          icon: iconBase + "culture.png",
        },
        food: {
          icon: iconBase + "food.png",
        },
        local: {
          icon: iconBase + "local.png",
        },
        lodging: {
          icon: iconBase + "lodging.png",
        },
        travel: {
          icon: iconBase + "travel.png",
        },
    };

    const features = locations_list; 

      // Create markers.
    for (let i = 0; i < features.length; i++) {
      const marker = new google.maps.Marker({
        position: features[i].position,
        icon: icons[features[i].type].icon,
        map: map,
      });
    }

    const input = document.getElementById("pac-input");
    const autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo("bounds", map);
    // Specify just the place data fields that you need.
    autocomplete.setFields(["place_id", "geometry", "name"]);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    const infowindow = new google.maps.InfoWindow();
    const infowindowContent = document.getElementById("infowindow-content");
    infowindow.setContent(infowindowContent);
    const marker = new google.maps.Marker({ map: map });
    marker.addListener("click", () => {
      infowindow.open(map, marker);
    });
    autocomplete.addListener("place_changed", () => {
      infowindow.close();
      const place = autocomplete.getPlace();

      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
      // Set the position of the marker using the place ID and location.
      marker.setPlace({
        placeId: place.place_id,
        location: place.geometry.location,
      });
      marker.setVisible(true);
      infowindowContent.children.namedItem("place-name").textContent =
        place.name;
      infowindowContent.children.namedItem("place-id").textContent =
        place.place_id;
      infowindowContent.children.namedItem("place-address").textContent =
        place.formatted_address;
      infowindow.open(map, marker);
      window.location = "/backoffice/business_edit?id=0&place_id=" + place.place_id;
    });
  }