$(document).ready(function() {
	$('.header__toggler').on('click', function() {
		if($(this).hasClass('open')) {
			$(this).removeClass('open').addClass('close')
			$('.header').addClass('open')
		} else {
			$(this).addClass('open').removeClass('close')
			$('.header').removeClass('open')
		}		
	})
})