var mySwiper = new Swiper('#brands-top', {
	loop: false,
	slidesPerView: 4,
	initialSlide: 2,
	breakpoints: {
		480: {
			slidesPerView: 3,
		},
		768: {
			slidesPerView: 5,
		},		
	}
})

var mySwiper = new Swiper('#brands-bot', {
	loop: false,
	slidesPerView: 2,
	centeredSlides: true,
	initialSlide: 2,
	breakpoints: {
		480: {
			slidesPerView: 3,
		},
		768: {
			slidesPerView: 4,
		}
	}
})

var mySwiper = new Swiper('#home-carousel', {
	loop: true,
	slidesPerView: 2,
	centeredSlides: true,
	breakpoints: {
		480: {
			slidesPerView: 3,
		},
		768: {
			slidesPerView: 4,
		}
	}
})

$(document).ready(function() {
	if($(this).scrollTop() > 0) {
		$('.header').addClass('fixed')
	}
	$(this).on('scroll', function() {
		if($(this).scrollTop() > 0) {
			$('.header').addClass('fixed')
		} else {
			$('.header').removeClass('fixed')
		}
	})

	$('#toggler').on('click', function() {
		$(this).toggleClass('open');
		$('.header__menu').toggleClass('show');
		$('.header').toggleClass('open');
	})

	$('.header__search-close').on('click', function() {
		$(this).toggleClass('open');
		$('.header__search').toggleClass('show');
	})

	$('.tab__title').each(function() {
		$(this).on('click', function() {
			const target = $(this).data('target');

			$(this).toggleClass('open');
			$('#'+target).toggleClass('open');

			// setTimeout(function() {				
			// 	$('#'+target).removeClass('collapsing');	
			// 	$('#'+target).toggleClass('open');
			// }, 1000)
		})
	})
})