<?php
if (isset($_GET['sendForm'])) {
    header('Content-type: application/json');
    if ($_POST['fullname'] == "" || $_POST['email'] == "" || $_POST['phone'] == "") {
        echo json_encode(array("res" => "NOK"));    
        die();
    }
    
    file_put_contents("/home/ubuntu/data/leads", json_encode($_POST) . "\n", FILE_APPEND);
    $html = "";
    $html .= 'Full Name: ' . $_POST['fullname'] . "\n";
    $html .= 'Phone: ' . $_POST['phone'] . "\n";
    $html .= 'Email: ' . $_POST['email'] . "\n";
    
    mail('sales@zero21.io', 'Lead Form 1', $html);
    // mail('ran@zero21.io', 'Lead Form 1', $html);
    
    echo json_encode(array("res" => "OK"));
    die();
}
?>
<!DOCTYPE html>
<html lang="he">
<head>
	<meta charset="UTF-8">
	<title>Zero21</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!-- Custom CSS -->
	<link rel="stylesheet" href="css/style.css">
    <script src="../assets/js/jquery-3.3.1.min.js"></script>
    <script>
    function sendForm() {
        $("#feeder").html('');
        $("#form-div").hide();
        $("#proc-div").show();
        $("#ty-div").hide();
        var req = {"fullname" : $("#fullname").val(), "phone" : $("#phone").val(), "email" : $("#email").val()};
        $.ajax({
            url : './index.php?sendForm',
            type : 'POST',
            data : req,
            success : function(ret) {
                if (ret.res == 'OK') {
                    $("#feeder").html('');
                    $("#form-div").hide();
                    $("#proc-div").hide();
                    $("#ty-div").show();
                    gtag('event', 'LP', {
                        'event_category' : 'conversion',
                        'event_label' : req.fullname + ' ' + req.phone + ' ' + req.email
                    });
                } else {
                    $("#feeder").html('אנא מלא את כל הפרטים');
                    $("#form-div").show();
                    $("#proc-div").hide();
                    $("#ty-div").hide();
                }
                console.log(ret);
            }
        });
    }
    </script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122456512-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-122456512-1');
        gtag('event', 'LP', {
          'event_category' : 'view',
          'event_label' : 'view'
        });
    </script>

</head>
<body>
	<header class="header">
		<h1 class="header__title">
			תוכנית יזמות מאפס לאחד
		</h1>

		<p class="header__description">
			<span>
				יזמים בנשמה? יש לכם רעיון שתמיד רציתם להגשים? יש לכם מיזם משלכם? 
			</span>
			<br>
			<span>
				בואו ללמוד לבנות עסק פעיל, חדשני ורווחי בתוכנית המקצועית ביותר בישראל!
			</span>
		</p>

		<a href="#scroll" class="header__button scroll">
			לחץ כאן להגשת מועמדות
		</a>
	</header>

	<div class="container">
		<div class="partners">
			<div class="partners__one">
				<img src="img/tau.jpg" alt="partner">
			</div>

			<div class="partners__group">
				<img src="img/logos.png" alt="partners">
			</div>
		</div>

		<div class="form" id="scroll">
            <div id="form-div">
                <h2>
                    השאירו פרטים ואנחנו כבר חוזרים
                </h2>
                <form onSubmit="sendForm(); return false;">
                    <input id="fullname" type="text" placeholder="שם מלא">
                    <input id="email" type="text" placeholder="טלפון">
                    <input id="phone" type="text" placeholder="כתובת מייל">
                    <div style="color: red; font-weight: bold; font-size: 2.4rem;" id="feeder"></div>
                    <input type="submit" name="submit" value="שלח!">
                </form>
            </div>
            <div id="proc-div" style="display: none; text-align: center; font-size: 2.4rem; padding: 100px;">
                שולח נתונים...
            </div>
            <div id="ty-div" style="display: none; text-align: center; font-size: 2.4rem; padding: 100px;">
                נשלח בהצלחה!
            </div>
			<hr class="line">
		</div>

		<div class="block">
			<h3 class="block__title">
				מה בתכנית?
			</h3>

			<p class="block__description">
                <p>
                    9 מתוך 10 עסקים שנפתחו בישראל נסגרו לפני שהצליחו להקים עסק מצליח ומרוויח.<br>
                </p>
                <p class="block__text-center">
                    <br>
                    <strong>
                    תכנית היזמות אפס לאחד שמה לעצמה מטרה להעלות את סיכוי ההצלחה שלכם להקמת עסק מצליח!
                    </strong>
                </p>
                <br>
                <p>
                    התכנית מקנה את כל סט הכלים השלם בין אם מדובר בידע, קשרים, כלים מעשיים, ליווי צמוד ובניית העסק תוך כדי התכנית –
                    איתנו תוכלו להפוך את החלום שלכם למציאות קיימת. <br>
                    התכנית נבנתה ועוצבה על מנת להקנות ליזמים ולמיזמים ערך מוסף משמעותי בצד האישי והעסקי – התכנית מקנה ידע הכרחי וכלים להקמה וניהול של עסק יזמי או עבודה במיזם בשלב מתקדם. 
                    התוכנית מספקת פלטפורמה לפיתוח מיזם עסקי משלב הרעיון ועד ההשקה והגיוס - משלבת הדרכה והכשרה במובן התיאורטי והמעשי ומקנה את הכלים המשמעותיים ביותר לכל יזם.
                </p>
			</p>

			<div class="block__img" style="padding: 3rem 0;">
				<img src="img/progress.png" alt="progress">
			</div>
		</div>

		<div class="block">
			<h3 class="block__title">
				יותר בהרחבה:
			</h3>

			<p class="block__description">
                <p>
                    הסטודנטים והיזמים עוברים תהליך שנוגע בנקודות המפתח החשובות ביותר להצלחה של הסטארטאפ עליו הם עובדים וכל זאת תוך כדי יישום והתנסות בבניית מיזם משלהם.
                    בתכנית נצא למסע יד ביד עם ליווי אישי וצמוד החל מרמת הרעיון, דרך המשאבים להם אתם זקוקים להוציא את הרעיון לפועל, בדיקת היתכנות עסקית, יצירת תכנית עסקית, תחזית צמיחה ורווחיות Go-no-go, גיוס כספים, גיוס אנשי מפתח ושותפים אסטרטגיים ועוד... 
                    התוכנית מועברת על ידי אנשי מפתח בתעשייה, יזמים ומשקיעים מנוסים תוך ליווי של מנטורים מובילים בתחומם ושל החברות הגדולות והמשמעותיות במשק.
                    מלווה בכלים ותהליכים בהשראת ה- Business Canvas Model - מתודיקה עסקית אשר הפכה לפורמט חובה בחברות סטרטאפ בעולם, מחליפה את התוכנית העסקית הישנה והמסורתית ומציבה רף חדש לחברות ההזנק.
                    בוגרי התכנית יקבלו את ההזדמנות להציג את המיזמים שלהם בפני משקיעים ולהתקדם לתכניות המשך באקסלרטורים המובילים בארץ ובעולם! 
                    <br><br>
                    קהל יעד:  <br>
                    סטודנטים שרוצים לעשות את הצעד הראשון שלהם בעולם העסקי עוד במהלך התואר <br>
                     יזמים המעוניינים להקים עסק חדש, כאלה שיש להם רעיון לעסק או נמצאים בשלב ההקמה הראשוני של העסק ומעוניינים להזניק אותו קדימה<br>
                    <br>
                    מטרת התוכנית: <br>
                    מטרת התכנית היא לקחת יזמים מתחילים וללוות אותם משלב הרעיון ועד לעסק פעיל, מרוויח ואפילו גיוס כספים ויציאה לחו"ל – כל זאת תוך מתן כל הכלים להם יזדקקו במהלך המסע וליווי של מנטורים מובילים מהתעשייה.<br>
                    מרעיון לעסק פעיל! הקמת עסק תוך כדי למידה. <br>
                    לימוד פרקטי של המודלים החשובים ביותר + התנסויות בשטח ברמה יישומית גבוה<br>
                    הקניית הכלים הנכונים והעדכניים ביותר להקמת סטארטאפ כולל ידע פיננסי ומשפטי הכרחי<br>
                    ליצור חשיפה ורשת קשרים שיעזרו ליזמים של המחר<br>
                    <br>

                    בין נושאי הקורס: <br>
                    גיבוש רעיון מהתיאוריה אל הפועל<br>
                    בחינת הרעיון על כל צדדיו ובעזרת מודלים מובילים <br>
                    סדנאות מוטיבציה – איך ליצור מוטיבציה בקרב עובדים? משקיעים? ועוד..<br>
                    יצירת ערך מוסף בר קיימה <br>
                    בניית תכנית עסקית ותכנית פיננסית <br>
                    איך מגייסים כסף למיזם? מקורות מימון ובניית תשתית<br>
                    שיווק ומכירות בסטארטאפים – בניית חומרים שיווקיים שיגרמו ללקוחות ומשקיעים לרדוף אחריכם <br>
                    כיצד מקימים עסק – משפטי ופרקטי<br>
                    מחקר שוק והכרת הלקוחות <br>
                    טכנולוגיה כמנוע להצלחה וחדשנות  <br>
                    בניית אב-טיפוס וחשיבה מחוץ לקופסה <br>
                    ימי עיון בחברות המובילות במשק <br>
                    אירוע סיום והצגת התוצרים אל מול משקיעים ובעלי עניין <br>
                    ועוד... <br><br>

                    <p class="block__text-center">
                        <p class="block__text-center">
                            <strong>השותפים שלנו כבר מחכים לכם עם: </strong><br><br>
                        </p>
                        <div class="block__img">
                            <img class="fluid-img" src="./img/aws.jpg"><br>
                        </div>
                        <p class="block__text-center">
                            שעות יעוץ חינם וקרדיטים באמזון AWS – עד 3000$ בחודש!
                        </p>
                        <br><br>
                        <div class="block__img">
                            <img src="./img/ey.png">
                        </div>
                        <p class="block__text-center">
                            יעוץ ומנטורינג אישי + פתיחת תיק בחינם בEY <br>
                        </p>
                        <br><br>
                        <div class="block__img">
                            <img src="./img/shibolet.png">
                        </div>
                        <p class="block__text-center">
                            יעוץ אישי וליווי בחינם בשיבולת
                        </p>
                    </p>
                </p>
			</p>
		</div>

		<a href="#scroll" class="more-info scroll">
			מעוניין לשמוע עוד פרטים?
		</a>

        
        <div class="block__img">
            <img src="img/faster.png" alt="faster">
        </div>
        
		<img src="img/flower.png" alt="flower" class="stickies flower">
		<img src="img/cogwheel-big.png" alt="cogwheel" class="stickies cogwheel-big">
		<img src="img/cogwheel-small.png" alt="cogwheel" class="stickies cogwheel-small">
	</div>

	<footer class="footer">
		<div class="footer__soc">
            <a href="https://www.linkedin.com/showcase/021innovaition/">
                <img src="img/linkedin.png" alt="linkedin">
            </a>
            <a href="https://www.facebook.com/021innovaition/">
                <img src="img/facebook.png" alt="facebook">
            </a>
		</div>

		<div class="footer__copyright">
			<span>Zero21</span>
		</div>
	</footer>

	<!-- Anchor animation -->
	<script>
		$(document).ready(function() {
			$('.scroll').on('click', function(){
				var anchor = $(this);
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: $(anchor.attr('href')).offset().top
				}, 500, 'linear');
			});
		});		
	</script>
	<!-- check if running in facebook app -->
	<script>
		var ua = navigator.userAgent || navigator.vendor || window.opera;
		function isFacebookApp() {
			return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
		}
		if (isFacebookApp()) {
			$("head").append($("<link rel='stylesheet' href='css/facebook_style.css' type='text/css' media='screen' />"));
		}
	</script>
</body>
</html>