let header = document.getElementById('header');
let toggler = header.querySelector('.header__toggler');
let menu = header.querySelector('.header__menu');

let dashToggler = header.querySelector('.header__account button');
let dashMenu = header.querySelector('.header__menu.dashboard');

toggler.onclick = function() {
	if(toggler.classList.contains('open'))	{
		toggler.classList.remove('open');
		toggler.classList.add('close');

		menu.classList.add('show')
	}	else {
		toggler.classList.remove('close');
		toggler.classList.add('open');

		menu.classList.remove('show')
	}
};

dashToggler.onclick = function() {
	dashMenu.classList.toggle('show');
};

let ua = navigator.userAgent || navigator.vendor || window.opera;
function isFacebookApp() {
	return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
}
if (isFacebookApp()) {
	document.head.insertAdjacentHTML("beforeend", "<link rel='stylesheet' href='css/facebook_style.css' type='text/css' media='screen' />");
}

// AIR DATEPICKER
(function ($) { $.fn.datepicker.language['en'] = {
	days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
	months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
	monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
	today: 'Today',
	clear: 'Clear',
	dateFormat: 'mm/dd/yyyy',
	timeFormat: 'hh:ii aa',
	firstDay: 0
}; })(jQuery);

// Initialization
$('.lecture-datepicker').datepicker({
	language: 'en',
	position: "top center",
})

// popup window
$(document).ready(function() {
	if($('div').is("#popup")) {
		let outPopup = $('#popup');

		$('#filter').on('click', function() {
			outPopup.addClass('show')
			$('body').addClass('noscroll')
		})

		$(this).on('click', function(e) {
			if(e.target.id == outPopup[0].id || e.target.type === 'submit') {
				outPopup.removeClass('show')
				$('body').removeClass('noscroll')
			}
		})
	}	
})
