$(document).ready(function(){
	// home page slider
	$('.intro-slider').slick({
		speed: 3000,
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		rtl: true,
	});

	// events page sliders
	const numberOfSliders = $(".events__row").length
	for(let i=1; i<=numberOfSliders; i++) {
		const speed = 3000 + i*10
		$(`.events__slider-${i}`).slick({
			speed: 3000,
			autoplay: true,
			autoplaySpeed: speed,
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			prevArrow: '<img src="../img/arrow-right.png" alt="arrow right" class="slick-prev">',
			nextArrow: '<img src="../img/arrow-left.png" alt="arrow left" class="slick-next">',
			responsive: [
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 1,
					arrows: false,
					dots: true
				}
			}]
		});
	}

	// about page - switchers (collapse effect)
	$('.academy__switch').on('click', function() {
		let ctgr = $(this).data('target');

		if(!$('#'+ctgr).hasClass('show')) {
			$('.academy__tab').removeClass('show');
			$('#'+ctgr).addClass('show');

			$('.academy__switch').removeClass('active');
			$(this).addClass('active');
		}
	})

	// gallery sliders (in popup)
	let gallerySlider = $('.gallery-slider');
	$('.gallery__image').on('click', function() {		
		$('.popup').addClass('show');
		$(gallerySlider).slick({
			speed: 2000,
			lazyLoad: 'progressive',
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: "<img class='slick-prev' alt='arrow right' src='../img/slider-arrow-left.svg'>",
			nextArrow: "<img class='slick-next' alt='arrow left' src='../img/slider-arrow-right.svg'>"
		});
	})

	$('#popupClose').on('click', function() {
		$(gallerySlider).slick('unslick');
		$('.popup').removeClass('show');
	})

	// show map - button
	$('.show-map').each(function() {
		$(this).on('click', function() {
			const mapId = $(this).data('map');
			$('.popup').addClass('show');
			$(`#${mapId}`).addClass('show');
		})
	})

	$('#popupClose').on('click', function() {
		$('.popup').removeClass('show');
		$('.contact-map').removeClass('show');
	})

	// header menu, on mobile
	$('.header__toggler').on('click', function() {
		$(this).toggleClass('open');
		$('.header__navbar').toggleClass('show');
	})
});