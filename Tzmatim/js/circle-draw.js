// Dashboard page
// script to draw "circle progress bars"
let qtyProgressBars = $('.circle-progress').length; 
for (let i = 1; i <= qtyProgressBars; i++) {
	let circleProgressValue = $(`#progress-${i} .circle-progress`).data('progress');
	let circleProgressLS = $(`#progress-${i} .circle-progress .circle-progress__left-side`)
	let circleProgressContainer = $(`#progress-${i} .circle-progress .circle-progress__container`)

	if(circleProgressValue >= 50) {
		circleProgressLS.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
	} else {
		circleProgressContainer.addClass('half');
		circleProgressLS.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
	}
}