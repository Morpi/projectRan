$(document).ready(function(){
	// Home page
	$('.header__toggler').on('click', function() {
		$(this).toggleClass('close')
		$('.header__navbar').toggleClass('show')

		if($('.header__navbar').hasClass('show')) {
			$('.header__navbar a').on('click', function() {
				$('.header__toggler').removeClass('close')
				$('.header__navbar').removeClass('show')
			})
			$('.header__navbar button').on('click', function() {
				$('.header__toggler').removeClass('close')
				$('.header__navbar').removeClass('show')
			})
		}
	})
	
	$('.popup-open').on('click', function() {
		$('#popup').addClass('show')
	})

	$('#popup-close').on('click', function() {
		$('#popup').removeClass('show')
	})

	$('.start-sidebar__toggler').on('click', function() {
		$(this).toggleClass('open')
		$('.start-sidebar').toggleClass('open')

		if($('.start-sidebar').hasClass('open')) {
			$('.start-sidebar__step').on('click', function() {
				$('.start-sidebar__toggler').removeClass('open')
				$('.start-sidebar').removeClass('open')
			})
		}
	})

	// Start pages
	let steps = $('.start-right__position div')
	for (let i = steps.length - 1; i > 0; i--) {
		$(steps[i]).css("right", `-${2*i}rem`)
	}

	// Swiper slider initialization 
	if($('.swiper-container').length !== 0) {
		var loginPromo = new Swiper('.login__promo-container .swiper-container', {
			loop: true,
			slidesPerView: 1,

			pagination: {
				el: '.swiper-pagination',
			},
		})

		var dashboardSlider = new Swiper('.dashboard-slider__container .swiper-container', {
			slidesPerView: 1,

			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})

		var dashboardCategorySlider = new Swiper('.dashboard-category__slider .swiper-container', {
			slidesPerView: 'auto',
			preventClicks : false,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})
	}

	// Dashboard page
	// script to draw "circle progress bars"
	let qtyProgressBars = $('.circle-progress').length;
	for (let i = 1; i <= qtyProgressBars; i++) {
		let circleProgressValue = $(`#progress-${i} .circle-progress`).data('progress');
		let circleProgressLS = $(`#progress-${i} .circle-progress .circle-progress__left-side`)
		let circleProgressContainer = $(`#progress-${i} .circle-progress .circle-progress__container`)
		
		if(circleProgressValue >= 50) {
			circleProgressLS.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
		} else {
			circleProgressContainer.addClass('half');
			circleProgressLS.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
		}
	}
	// menu toggler
	$('.sidebar-mobile__toggler').on('click', function() {
		$('.sidebar').toggleClass('open')
	})
	// dropdown toggler
	$('.dashboard-dropdown').on('click', function() {
		$(this).toggleClass('open')
	})
	// progress bar dropdown toggler
	$('.dashboard-home__element-data').on('click', function() {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open')
		} else {
			$('.dashboard-home__element-data').removeClass('open')
			$(this).addClass('open')
		}
	})

	//---dashboard mobile togglers---//
	$('.sidebar-mobile__search-btn').on('click', function() {
		$('.dashboard-search__mobile').toggleClass('open')
		$('.sidebar-mobile__toggler-search').addClass('show')
		$(this).toggleClass('close')
	})
	$('.sidebar-mobile__toggler-search').on('click', function() {
		$(this).removeClass('show')
		$('.dashboard-search__mobile').removeClass('open')
		$('.sidebar-mobile__search-btn').removeClass('close')
	})

	// chat page
	// chat togglers 
	$('.dashboard-notifications__users-element').on('click', function() {
		$(this).toggleClass('active')
		$('.dashboard-chat').toggleClass('open')
	})
	$('.dashboard-chat__deployed-back').on('click', function() {
		$('.dashboard-notifications__users-element').removeClass('active')
		$('.dashboard-chat').removeClass('open')
	})
	// chat group popup
	if($('.dashboard-chat__deployed').hasClass('group-chat')) {
		$('.dashboard-chat__deployed-who img').on('click', function() {
			$('.dashboard-chat__popup').addClass('open')
		})
		$('.dashboard-chat__popup-close').on('click', function() {
			$('.dashboard-chat__popup').removeClass('open')
		})
	}

	// alerts toggler
	$(window).on('resize load', function() {
		if(this.matchMedia("(max-width: 767px)").matches) {
			$('#alerts-mobile').removeClass('active')
			$('.dashboard-alerts').removeClass('show')
			$('.sidebar').removeClass('alerts')
		}
	})
	$('#alerts-mobile').on('click', function(e) {
		$(this).toggleClass('active')
		$('.dashboard-alerts').toggleClass('show')
		$('.sidebar').toggleClass('alerts')
	})
	
	// category page
	// category mobile menu handler
	$(window).on('scroll', function() {
		if($(window).scrollTop() > 0) {
			$('.sidebar-category__mobile').addClass('hide')
		} else {
			$('.sidebar-category__mobile').removeClass('hide')
		}
	})
	$('.sidebar-category__mobile-toggler').on('click', function() {
		$('.sidebar-category').toggleClass('open')
	})
	// video handler
	$('.dashboard-category__popup-video').on('click', function() {
		$(this).addClass('video-play');
		$('.dashboard-category__popup-video iframe')[0].src += "?autoplay=1";
	})
	// category popup handler
	$('.dashboard-category__extra-element').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target')
			$(`#${target}`).addClass('open')
		})
	})
	$('.dashboard-category__popup-close').on('click', function() {
		$('.dashboard-category__popup').removeClass('open')
	})
	// input handler
	$('.dashboard-category__checkbox').each(function() {
		$(this).on('click', function(e) {
			if(e.target.tagName === 'INPUT') {
				$(this).toggleClass('done')
			}
		})
	})

	// community page
	// community tabs handler
	$('.dashboard-community__tab').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.dashboard-community__tab').removeClass('active')
			$(this).addClass('active')
			$('.dashboard-community__content-tab').removeClass('active')
			$(`#${target}`).addClass('active')
		})
	})
	// community dropdown handler
	$('.dashboard-community__dropdown').each(function() {
		$(this).on('click', function() {
			if($(this).hasClass('open')) {
				$('.dashboard-community__dropdown').removeClass('open');
			} else {
				$('.dashboard-community__dropdown').removeClass('open');
				$(this).toggleClass('open');
			}		
		})
	})
	// community popup handler
	$('.dashboard-community__groups-help').on('click', function() {
		$('.dashboard-popup').addClass('open');
	})
	$('.dashboard-popup__close').on('click', function() {
		$('.dashboard-popup').removeClass('open');
	})
	$('.dashboard-community__filter-btn').on('click', function() {
		$('.dashboard-community__filter-dropdowns').addClass('open');
	})
	$('.dashboard-community__filter-close').on('click', function() {
		$('.dashboard-community__filter-dropdowns').removeClass('open');
	})
	
	// services page
	// services popup handler
	$('.dashboard-services__card-action button').on('click', function() {
		$('.dashboard-popup').addClass('open');
	})
	$('.dashboard-popup__close').on('click', function() {
		$('.dashboard-popup').removeClass('open');
	})

	// faq page
	// faq hamburger & tabs handlers
	$('.dashboard-faq__tab').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.dashboard-faq__tab').removeClass('active')
			$(this).addClass('active')
			$('.dashboard-faq__content-tab').removeClass('active')
			$(`#${target}`).addClass('active')
		})
	})
	$('.dashboard-faq__hamburger-title').each(function() {
		$(this).on('click', function() {
			$(this).toggleClass('open')
		})
	})

	// tasks page
	// script to draw "circle progress bars"
	let qtyTasksProgressBars = $('.tasks-progress').length;
	for (let i = 1; i <= qtyTasksProgressBars; i++) {
		let circleProgressValue = $(`#tasks-progress-${i}`).data('progress');
		if(circleProgressValue >= 50) {
			$(`#tasks-progress-${i} .tasks-progress__left-side`)
			.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
		} else {
			$(`#tasks-progress-${i} .tasks-progress__container`).addClass('half');
			$(`#tasks-progress-${i} .tasks-progress__left-side`)
			.css('transform', `rotate(${3.6 * circleProgressValue}deg)`);
		}
	}
	// tasks popup handler
	$('.dashboard-tasks__tip').on('click', function() {
		$('.dashboard-tasks__popup').addClass('open');

		var tasksSlider = new Swiper('.tasks-slider.swiper-container', {
			slidesPerView: 1,

			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})
	})
	$('.dashboard-tasks__popup-close').on('click', function() {
		$('.dashboard-tasks__popup').removeClass('open');
	})

	// user profile page
	// user profile tabs handler
	$('.dashboard-profile__tab').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.dashboard-profile__tab').removeClass('active')
			$(this).addClass('active')
			$('.dashboard-profile__content-tab').removeClass('active')
			$(`#${target}`).addClass('active')
		})
	})
	// user profile dropdown handler
	$('.dashboard-profile__user-dropdown').each(function() {
		$(this).on('click', function() {
			if($(this).hasClass('open')) {
				$('.dashboard-profile__user-dropdown').removeClass('open');
			} else {
				$('.dashboard-profile__user-dropdown').removeClass('open');
				$(this).toggleClass('open');
			}		
		})
	})
})
