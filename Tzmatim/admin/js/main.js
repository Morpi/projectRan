$(document).ready(function() {
	if($('.admin-reports__chart').length !== 0) {
		new Chart(document.getElementById("chart1"), {
			type: 'doughnut',
			data: {
				labels: [
				"15% - מיסוי",
				"20% - טכנולוגיה",
				"30% - החלטות פיננסיות",
				"15% - ביטוח",
				"20% - שיווק ורשתות חברתיות"],
				datasets: [
				{
					label: "Population (millions)",
					backgroundColor: ["#EB5757", "#56CCF2","#BB6BD9","#F2C94C","#27AE60"],
					data: [15,20,30,15,20],
					width: 1,
				}
				]
			},
			options: {
				maintainAspectRatio: false,
				cutoutPercentage: 70,
				legend: {
					rtl: true,
					position: 'bottom',
					labels: {
						fontSize: 14,
						fontColor: '#80879F',
						boxWidth: 10,
						usePointStyle: true,
					},
				},
				elements: {
					center: {
						text: 'Red is 2/3 of the total numbers',
						color: '#FF6384',
						sidePadding: 20,
						minFontSize: 25,
						lineHeight: 25
					}
				}
			}
		});

		new Chart(document.getElementById("chart2"), {
			type: 'line',
			data: {
				labels: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוטס','ספטמבר','אוקטובר', 'נובמבר', 'דצמבר'	],
				datasets: [{ 
					data: [20000, 60000, 30000, 40000, 60000, 70000, 60000, 20000, 90000, 60000, 30000, 40000,],
					label: "פניות למומחה",
					borderColor: "#56CCF2",
					fill: false
				}, { 
					data: [78000, 100000, 70000, 83000, 105000, 63000, 60000, 65000],
					label: "פניות לקהילה",
					borderColor: "#F2994A",
					fill: false
				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					rtl: true,
					position: 'bottom',
					labels: {
						fontSize: 14,
						fontColor: '#80879F',
						boxWidth: 10,
						usePointStyle: true,
					},
				},
				scales: {
					yAxes: [{
						stacked: false,
						gridLines: {
							display: true,
							color: "#CCCFE3"
						},
						ticks: {
							beginAtZero: true,
							stepSize: 20000,
						}
					}],
					xAxes: [{
						gridLines: {
							display: false,
						}
					}]
				}
			}
		});

		new Chart(document.getElementById('chart3'), {
			type: 'bar',
			data: {
				labels: ["שיווק ורשתות חבריות", "מיסוי", "פיננסים", "טכנולוגיה", "ביטוח",],
				datasets: [
				{
					label: "פניות לקהילה",
					backgroundColor: "#2F80ED",
					barThickness: 30,
					maxBarThickness: 10,
					data: [15000, 25000, 35000, 65000, 55000, 100000],

				},
				{
					label: "פניות למומחה",
					backgroundColor: "#27AE60",
					barThickness: 10,
					maxBarThickness: 10,
					data: [20000, 30000, 40000, 70000, 60000, 100000],

				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					rtl: true,
					position: 'bottom',
					labels: {
						fontSize: 14,
						fontColor: '#80879F',
						boxWidth: 10,
						usePointStyle: true,
					},
				},
				scales: {
					yAxes: [{
						stacked: false,
						gridLines: {
							display: true,
							color: "#CCCFE3"
						},
						ticks: {
							beginAtZero: true,
							stepSize: 20000,
						}
					}],
					xAxes: [{
						gridLines: {
							display: false,
						}
					}]
				}
			}
		});

		new Chart(document.getElementById('chart4'), {
			type: 'bar',
			data: {
				labels: ["שיווק ורשתות חבריות", "מיסוי", "פיננסים", "טכנולוגיה", "ביטוח",],
				datasets: [
				{
					label: "פניות לקהילה",
					backgroundColor: "#5FBDBD",
					barThickness: 30,
					maxBarThickness: 10,
					data: [15000, 25000, 35000, 65000, 55000, 100000],

				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: false,
				},
				scales: {
					yAxes: [{
						stacked: false,
						gridLines: {
							display: true,
							color: "#CCCFE3"
						},
						ticks: {
							beginAtZero: true,
							stepSize: 20000,
						}
					}],
					xAxes: [{
						gridLines: {
							display: false,
						}
					}]
				}
			}
		});

		new Chart(document.getElementById('chart5'), {
			type: 'bar',
			data: {
				labels: ["שיווק", "מיסוי", "פיננסים", "טכנולוגיה", "ביטוח",],
				datasets: [
				{
					label: "פניות לקהילה",
					backgroundColor: "#27AE60",
					barThickness: 30,
					maxBarThickness: 10,
					data: [15000, 25000, 35000, 65000, 55000, 100000],

				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					rtl: true,
					position: 'bottom',
					labels: {
						fontSize: 14,
						fontColor: '#80879F',
						boxWidth: 10,
						usePointStyle: true,
					},
				},
				scales: {
					yAxes: [{
						stacked: false,
						gridLines: {
							display: true,
							color: "#CCCFE3"
						},
						ticks: {
							beginAtZero: true,
							stepSize: 20000,
						}
					}],
					xAxes: [{
						gridLines: {
							display: false,
						}
					}]
				}
			}
		});
	}

	if($('.swiper-container').length !== 0) {
		var dashboardCategorySlider = new Swiper('#category-slider-1 .swiper-container', {
			slidesPerView: 'auto',

			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})
		var dashboardCategorySlider = new Swiper('#category-slider-2 .swiper-container', {
			slidesPerView: 'auto',

			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})

		var dashboardCategorySlider = new Swiper('.admin-q__header .swiper-container', {
			slidesPerView: 'auto',
		})
	}

	if($('#editor-add-question').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-question', {
			theme: 'snow',
			placeholder: 'תשובה',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-article').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-article', {
			theme: 'snow',
			placeholder: 'תשובה',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-video').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-video', {
			theme: 'snow',
			placeholder: 'תשובה',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-tip').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-tip', {
			theme: 'snow',
			placeholder: 'תשובה',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-review').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-review', {
			theme: 'snow',
			placeholder: 'תשובה',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-customer-review').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-customer-review', {
			theme: 'snow',
			placeholder: 'הוסף את הביקורת',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-opinion').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-opinion', {
			theme: 'snow',
			placeholder: 'הוסף את הביקורת',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-q-open').length !== 0) {
		var editorAddQuestion = new Quill('#editor-q-open', {
			theme: 'snow',
			placeholder: 'placeholder',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}

	if($('#editor-add-professionals').length !== 0) {
		var editorAddQuestion = new Quill('#editor-add-professionals', {
			theme: 'snow',
			placeholder: 'תיאור כללי על בעל המקצוע',
			modules: {
				toolbar: [
				'bold',
				'italic',
				'underline',
				'strike',
				{ 'font': [] },
				{ 'header': [1, 2, 3, 4, 5, 6, false] },
				{ 'color': [] },
				{ 'align': [] },
				{ 'list': 'ordered'},
				{ 'list': 'bullet' },
				{ 'script': 'sub'},
				{ 'script': 'super' },
				'image'
				]
			}
		});
	}	

	// admin pages
	// sidebar hamburger handler
	$('.admin-sidebar__title').each(function() {
		$(this).on('click', function() {
			$(this).toggleClass('open')
		})
	})

	// admin login popup handler
	$('.admin-add').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target')
			$(`#${target}`).addClass('open')
		})
	})
	$('.admin-editor__close').on('click', function() {
		$('.admin-editor').removeClass('open')
	})

	// admin editor popup handler
	$('.header-right__dropdown-user').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target')
			$(`#${target}`).addClass('open')
		})
	})
	$('.admin-category__extra-element').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target')
			$(`#${target}`).addClass('open')
		})
	})
	$('.admin-popup__close').on('click', function() {
		$('.admin-popup').removeClass('open')
	})

	// admin home page hamburger & tabs handlers
	$('.admin-home__tab').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.admin-home__tab').removeClass('active')
			$(this).addClass('active')
			$('.admin-home__content-tab').removeClass('active')
			$(`#${target}`).addClass('active')
		})
	})
	$('.admin-home__hamburger-title').each(function() {
		$(this).on('click', function() {
			$(this).toggleClass('open')
		})
	})	

	// admin tips tabs handler
	$('.admin-tips__tab').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.admin-tips__tab').removeClass('active')
			$(this).addClass('active')
			$('.admin-tips__content-tab').removeClass('active')
			$(`#${target}`).addClass('active')
		})
	})

	// admin q pages tabs handler
	$('.admin-editor__tabs .admin-editor__radio-box').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$('.admin-editor__tab').removeClass('open')
			$(`#${target}`).addClass('open')
		})
	})

	// admin groups page popup handler
	$('.admin-groups__element-add').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$(`#${target}`).addClass('open')
		})
	})

	// admin services page popup handler
	$('.admin-services__element-menu button').each(function() {
		$(this).on('click', function() {
			let target = $(this).data('target');
			$(`#${target}`).addClass('open')
		})
	})
	$('.admin-services__element-add').on('click', function() {
		let target = $(this).data('target');
		$(`#${target}`).addClass('open')
	})

	// admin professionals page handlers
	$('.admin-professionals__dropdown').each(function() {
		$(this).on('click', function() {
			if($(this).hasClass('open')) {
				$('.admin-professionals__dropdown').removeClass('open');
			} else {
				$('.admin-professionals__dropdown').removeClass('open');
				$(this).toggleClass('open');
			}		
		})
	})
	$('.admin-professionals__element-add').on('click', function() {
		let target = $(this).data('target');
		$(`#${target}`).addClass('open')
	})
})
