$(document).ready(function() {
	$('.header__toggler').on('click', function() {
		if($(this).hasClass('open')) {
			$(this).addClass('close').removeClass('open')
			$('.navbar').addClass('open')
		} else {
			$(this).addClass('open').removeClass('close')
			$('.navbar').removeClass('open')
		}
	})

	$(".navbar__links a").on("click", function() {
		let toggler = $('.header__toggler');
		if(toggler.hasClass('open')) {
			toggler.addClass('close').removeClass('open')
			$('.navbar').addClass('open')
		} else {
			toggler.addClass('open').removeClass('close')
			$('.navbar').removeClass('open')
		}
	})
})